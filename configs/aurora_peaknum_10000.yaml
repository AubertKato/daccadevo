# The random seed
#seed: 42

keepTemporaryFiles: False

features_list: []
fitness_type: "peaksNumber"

peaksNumberDomain: [0.0, 1.0]
peaksLastValueDomain: [0.0, 1.0]
oscillationsDomain: [0.0, 1.0]
averagePeriodDomain: [0.0, 1.0]
valueStdDomain: [0.0, 1.0]
periodStdDomain: [0.0, 1.0]


# The name of the main algorithm (see below the description of 'algoTotal')
main_algorithm_name: algoDACCAD

daccad:
    path: "../daccad"
    env_name: "oscillator"
    config_file: "test.conf"

# The list of all container.
# The list of all container.
containers:
    parentContainer:                             # We create a container that will contain ALL tested individuals
        type: Container                          # A simple container, that will store ALL added individuals that are within bounds
        name: parentContainer                    # The name of the container. Optional. Default to the parent key (here also 'cont0')
        storage_type: orderedset                 # Faster than 'list' for large containers
        #storage_type: indexableset              # Faster than 'orderedset' but may have compatibility problems with pickle in some versions
        fitness_domain:                          # The domain of each fitness objective. Here individuals with any fitness domain are accepted.
        features_domain:                         # The initial domain of each feature. Here, no values is specified: it means all individuals are accepted, with any kind of features descriptor domains.

    cont0:                                                # The main grid container
        type: AutoScalingGrid                             # The type of the container. Here we use an AutoScalingGrid that periodically adjust the feature descriptors domains to match the individuals encountered so far
        name: cont0                                       # The name of the container. Optional. Default to the parent key (here also 'cont0')
        parents: [parentContainer]                        # The list of parent containers. Every individual added to `cont0` will be also forwarded to the parents
        scaling_containers: [parentContainer]             # The list of containers used to store individuals added after a rescaling operation. If empty, will use the parents instead.
        fitness_scaling: False                            # Whether to autoscale the fitness or not
        features_scaling: True                            # Whether to autoscale the features or not
        rescaling_period: 1000                            # When to perform the autoscaling operation. Here it's done every time 1000 individuals are added.
        shape: [25, 25]                                   # The number of bins for each feature
        max_items_per_bin: 1                              # The number of items in each bin of the grid
        fitness_domain: [[0., 1.]]                        # The domain of each fitness objective (here we only have one objective). Must be specified here even if the container will autoscale the domains, to set the number of fitness
        features_domain: [[-10.0, 10.0], [-10.0, 10.0]]   # The initial domain of each feature. Must be specified here even if the container will autoscale the domains, to set the number of features

    cont1:                                                # A feature-extracting container decorator applied to the main grid
        type: TorchFeatureExtractionContainerDecorator    # The type of the container. This decorator type uses PyTorch models (autoencoders) as a feature reduction method
        container: cont0                                  # The container this decorator is applied to
        training_containers: [parentContainer]            # The list of containers used to train the autoencoder. If empty, will use the parents instead.
        base_scores: ["peaksLastValue", "averagePeriod", "valueStd", "periodStd"]                 # Optional: the names of the scores used as input of the autoencoder. By default will use all non-extracted scores (including the performance/fitness).
        initial_nb_epochs: 100                            # The number of epochs used the first time the model is optimised
        nb_epochs: 30                                     # The number of epochs used for subsequent training processes
        learning_rate: 1.e-3                              # Learning rate of the training (by default, the Adam optimiser is used to train the model)
        training_period: 2000                             # Re-train the model every time `training_period` add operations are performed





# The list of all algorithms
algorithms:
    # Default parameter values for each algorithm
    optimisation_task: maximisation   # We perform maximisation of all fitness objectives
    dimension: 0                      # The number of dimensions of the problem. For rastrigin, any dimension >= 2 can be chosen
    ind_domain: [0., 1.]              # The domain of each value of the genome (optional)
    container: cont1                  # The container to use to store individuals told to the optimisers

    # Then, we use an evolutionary algorithm that perform random search and polynomial mutations. This algorithm makes a trade-off between quality (finding good performing solutions) and diversity (find solutions corresponding to each bin of the grid)
    algoDACCAD:
        type: DaccadBioneatMut
        budget: 10000       # The total number of allowed evaluations for this algorithm
        batch_size: 400     # The number of evaluations in each subsequent batch 
        sel_pb: 0.9         # The probability of performing selection+variation instead of initialising a new genome
        init_pb: 0.1        # The probability of initiating a new genome instead of performing selection
        mut_pb: 0.4         # The probability of mutating each value of the genome of a selected individual
        eta: 20.            # The ETA parameter of the polynomial mutation (as defined in the origin NSGA-II paper by Deb.). It corresponds to the crowding degree of the mutation. A high ETA will produce mutants close to its parent, a small ETA will produce offspring with more changes.
        init_drift: 10     # Number of random mutations applied to base individuals
        min_ind_found_in_init: 4
